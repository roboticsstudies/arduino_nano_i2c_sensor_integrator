int vcc  = PD4;            // attach pin D4 to vcc
int trig = PD5;            // attach pin D5 to Trig
int echo = PD6;            // attach pin D6 to Echo
int gnd  = PD7;            // attach pin D7 to GND

void setup()
{
pinMode (vcc,OUTPUT);     // define pin vcc as output
pinMode (gnd,OUTPUT);     // define pin gnd as output
pinMode (trig, OUTPUT);   // define pin trig as output
pinMode (echo,INPUT);     // define pin echo as input
digitalWrite (vcc,HIGH);  // writes +5V to the vcc pin
digitalWrite (gnd,LOW);   // writes +0V to the gnd pin
Serial.begin (9600);      // initialize serial communication:
}

void loop()

{
long duration, inches, cm;

// The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
// Give a short LOW pulse beforehand to ensure a clean HIGH pulse:

digitalWrite      (trig, LOW);
delayMicroseconds (2);
digitalWrite      (trig, HIGH);
delayMicroseconds (5);
digitalWrite      (trig, LOW);

// The same pin is used to read the signal from the PING))): a HIGH
// pulse whose duration is the time (in microseconds) from the sending
// of the ping to the reception of its echo off of an object.

duration = pulseIn (echo, HIGH);

// convert the time into a distance
inches = microsecondsToInches(duration);
cm = microsecondsToCentimeters(duration);

Serial.print  (inches);
Serial.print  ("in, ");
Serial.print  (cm);
Serial.print  ("cm");
Serial.println();

delay(100);
}

long microsecondsToInches(long microseconds)

{
// According to Parallax's datasheet for the PING))), there are
// 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
// second). This gives the distance travelled by the ping, outbound
// and return, so we divide by 2 to get the distance of the obstacle.
// See: http://www.parallax.com/dl/docs/prod/acc/28015-PI...

return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds)

{
// The speed of sound is 340 m/s or 29 microseconds per centimeter.
// The ping travels out and back, so to find the distance of the
// object we take half of the distance travelled.

return microseconds / 29 / 2;
}
