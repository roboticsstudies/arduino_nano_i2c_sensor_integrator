# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Este repositório é para organizar os códigos de Arduino Nano para integrar sensores na plataforma aérea da SensorVision.

A ideia é controlar sensores simples com o Nano e enviar apenas os resultados dos sensores por I²C à Pixhawk2.

### How do I get set up? ###

Arranje um Arduino Nano, sensores, uma placa de pão e um cabo mini-USB.
Baixe o Arduino IDE do site oficial ou use o IDE online, se você for desse tipo de gente.

### Contribution guidelines ###

Se o código compilar e fizer sentido, pode colocar.
Lembrando que é apenas código para o integrador de sensores para a plataforma aérea.
Considere criar um outro repositório caso sua contribuição fuja do escopo desta aplicação.

### Who do I talk to? ###

jonathas@sensorvision.com.br